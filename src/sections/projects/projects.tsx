import React, { useState } from "react";
import SectionTitle from "../../components/section-title/section-title";
import ProjectCard, {
  ProjectCardItem,
} from "../../components/project-card/project-card";
import "./projects.scss";
import { CSSTransition } from "react-transition-group";
import OpenProject from "../../components/open-project/open-project";
import { useInView } from "react-intersection-observer";
import classNames from "classnames";

const projectsData: ProjectCardItem[] = [
  {
    name: "AppForrest",
    companyDescription: "CRM для ресторанов",
    stack: ["React js", "Redux", "SCSS", "Thunk"],
    team:
      "Дизайнер, продуктолог, продукт менеджер, аналитик, 2 тестировщика, \n" +
      "4 бэкенд разработчика, 2 фронтендера",
    tasks:
      "Настройка проекта, создание UI kit, реализация основных страниц, \n" +
      "проведение codeReview, gitflow.",
    links: ["https://www.appforrest.ru"],
    cardLink: "https://www.appforrest.ru",
    shortDescription:
      "CRM для ресторанов\n" +
      "\n" +
      "Введение проекта с нуля и до релиза, разработка UI kit и основных страниц.",
    id: 0,
  },
  {
    name: "Zharpizza",
    companyDescription: "Крупная ресторанная сеть.",
    stack: ["Vanila js", "Fastify", "Nunjucks"],
    team:
      "Дизайнер, продуктолог, продукт менеджер, аналитик,  тестировщик, \n" +
      "2 бэкенд разработчика, поначал был еще один фронтэнд разработчик, в дальнейшем остался только я один.",
    tasks:
      "Разработка UI основного сайта и бэкофиса. Доработка и создание новых компонентов UI, работа с корзиной, меню ресторана,  создание лендинга на 20 летие сети.",
    links: ["https://zharpizza.ru", "https://zharpizza.ru/20-let"],
    cardLink: "https://zharpizza.ru",
    shortDescription:
      "Крупная ресторанная сеть.\n" +
      "\n" +
      "Работал над UI (как над самим сайтом так и backoffice), разработка корзины и меню.",
    id: 1,
  },
  {
    name: "GuavaPay",
    companyDescription: "Решения для отплаты и приема платажей.",
    stack: ["React js", "Redux", "RTK", "Typescript"],
    team:
      "Дизайнер, продукт менеджер, продуктолог, тестировщик, аналитик, \n" +
      "5 бэкенд разработчика и я один фронтенд",
    tasks:
      "Разработка backoffice сайта, с разными ролями, с различными разделами \n " +
      "разработка библиотеки компонентов, создание многостраничного сайта-визитки.",
    links: [
      "https://guavapay.com",
      "https://guavapay.com/company",
      "https://guavapay.com/individuals",
    ],
    cardLink: "https://guavapay.com",
    shortDescription:
      "Решения для отплаты и приема платажей.\n" +
      "\n" +
      "Разработка backoffice сайта, с разными ролями и множеством разделов.",
    id: 2,
  },
  {
    name: "Fairmay",
    companyDescription: "Британский Banking.",
    stack: ["React js", "Redux Saga", "Typescript"],
    team:
      "Тестировщик, дизайнер, аналитик, продукт менеджер, продуктолог, \n" +
      "3 бэкенд разработчика, 3 фронтенд разработчика",
    tasks:
      "Принятие участие в разработке UI kit, верстка и сборка разделов сайта, \n " +
      "настройка API flow в проекте, проведение и участие в codeReview",
    links: ["https://fairmay.com/", "https://fairmay.com/about/"],
    cardLink: "https://fairmay.com/",
    shortDescription:
      "Британский Banking.\n" +
      "\n" +
      "Сервис по приему и отправки платежей, ориентирован на британскую аудитория, но действует по всему миру.",
    id: 3,
  },
  {
    name: "Alarma",
    companyDescription: "Сервис для работы с Amazon.",
    stack: ["React", "Redux", "Typescript", "Graphql"],
    team:
      "Продукт менеджер, продуктолог, дизайнер, тестировщик, аналитик,  \n" +
      "2 бэкенд разработчика, один фронтенд",
    tasks:
      "Разработка UI части для бэкофиса, UI kit, настройка проекта с нуля, \n" +
      "codereview, gitflow, typescript config, docker file",
    links: ["https://www.alrm.online/"],
    cardLink: "https://www.alrm.online/",
    shortDescription:
      "Сервис для работы с Amazon.\n" +
      "\n" +
      "Сервис для отсужевания товаров, их фильтрация и разбиение по категориям.",
    id: 4,
  },
  {
    name: "Mash and the Bear",
    companyDescription: "Ресторан по мотивам мультфильма",
    stack: ["React js", "SCSS", "Graphql"],
    team:
      "Дизайнер, продуктолог, продукт менеджер, аналитик,  тестировщик, \n" +
      "4 бэкенд разработчика, 2 фронтенд разработчика",
    tasks:
      "Принятие участие в разработке как основного сайта так и backoffice, \n" +
      "разработка UI kit, настройка оплаты, авторизации, профиль пользователя, отложенные заказы.",
    links: [
      "https://restaurant.mashabear.com/",
      "https://restaurant.mashabear.com/shop/",
    ],
    cardLink: "https://restaurant.mashabear.com/",
    shortDescription:
      "Ресторан в Дубае Маша и медведь\n" +
      "\n" +
      "Разработка сайта для клиентов и для администрирования рестораном.",
    id: 5,
  },
];

const Projects = () => {
  const { ref, entry } = useInView({
    threshold: 0.1,
    triggerOnce: true,
  });

  const [activeProject, setActiveProject] = useState<
    ProjectCardItem | undefined
  >(undefined);
  const [visibleProject, setVisibleProject] = useState(false);

  console.log(entry?.isIntersecting);

  return (
    <>
      <div
        className={classNames("projects", {
          "fade-show": entry?.isIntersecting,
        })}
        id="projects"
        ref={ref}
      >
        <div className="container">
          <SectionTitle
            sectionNumber="03."
            sectionText="Projects"
            withoutLine
          />
          <div className="projects__items">
            {projectsData.map((item, index) => (
              <ProjectCard
                key={index}
                {...item}
                onClick={(project: ProjectCardItem) => {
                  setVisibleProject(true);
                  setActiveProject(project);
                }}
              />
            ))}
          </div>
        </div>
      </div>
      <OpenProject
        project={activeProject}
        visible={visibleProject}
        onClose={() => setVisibleProject(false)}
      />
    </>
  );
};

export default Projects;
