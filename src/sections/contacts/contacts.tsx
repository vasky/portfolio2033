import React, {
  LegacyRef,
  ReactElement,
  RefObject,
  useEffect,
  useRef,
  useState,
} from "react";
import mailIcon from "./../../assets/images/mail-icon.svg";
import tgIcon from "./../../assets/images/tg-icon.svg";

import "./contacts.scss";
import Button from "../../components/button/button";
import { useInView } from "react-intersection-observer";
import classNames from "classnames";

const Contacts = () => {
  const { ref, entry } = useInView({
    threshold: 0.5,
    triggerOnce: true,
  });

  return (
    <div
      id="contacts"
      ref={ref}
      className={classNames("contacts", {
        "fade-show": entry?.isIntersecting,
      })}
    >
      <div className="contacts__title">Get In Touch</div>
      <div className="contacts__text">
        Если у вас есть вопросы или предложения по работе, то пишите на почту
        или в tg.
      </div>
      <ul className="contacts__list">
        <li className="contacts__item">
          <img src={mailIcon} alt="mail" className="contacts__icon" />
          <a href="mailto: skyva29@gmail.com" className="contacts__item--mail">
            skyva29@gmail.com
          </a>
        </li>
        <li className="contacts__item">
          <img src={tgIcon} alt="telegram" className="contacts__icon" />
          <a
            href="tg://resolve?domain=vavasky"
            className="contacts__item--telegram"
            target={"_blank"}
          >
            @vavasky
          </a>
        </li>
      </ul>

      <a
        href="tg://resolve?domain=vavasky"
        className="link--without-underline"
        target={"_blank"}
      >
        <Button text="Сказать Привет" />
      </a>
    </div>
  );
};

export default Contacts;
