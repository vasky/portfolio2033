import React, { useEffect, useRef, useState } from "react";
import "./about.scss";
import SectionTitle from "../../components/section-title/section-title";
import { DotsAbout } from "./dots-about";
import avatarImage from "../../assets/images/avatar.png";
import { useInView } from "react-intersection-observer";
import classNames from "classnames";

const listItems = [
  "React",
  "Docker",
  "TypeScript",
  "Next js",
  "JavaScript (ES6, ES8 +)",
  "Webpack",
];

const About = () => {
  const { ref, entry } = useInView({
    threshold: 0.5,
    triggerOnce: true,
  });

  return (
    <div
      className={classNames("about", {
        "fade-show": entry?.isIntersecting,
      })}
      id="about"
      ref={ref}
    >
      <div className="container">
        <SectionTitle sectionText="About me" sectionNumber="01." />
        <div className="about__content">
          <div className="about__text-part">
            <p>
              Привет! Меня зовут Вадим, и мне нравится программировать, особенно
              когда это доступно каждому - в интернете. Мой интерес к
              веб-разработке начался в 2016 году, когда я поступил в колледж,
              параллельно сталкивался с ресурсами, где люди создавали
              потрясающие веб-страницы и в совокупности, эти вещи, определили
              мое направление.
            </p>
            <p>
              Я имел честь работать: в небольшой команде сервисного центра,{" "}
              <a href="https://mediamalina.ru/" target="_blank">
                в рекламном агенстве
              </a>{" "}
              (с довольно продуктивной командой), немного в большой корпорации (
              <a href="https://kontur.ru/" target="_blank">
                SKB Kontur
              </a>
              ). В настоящий момент, я работаю в компани{" "}
              <a href="https://www.hiplabs.dev/" target="_blank">
                HipLabs
              </a>
              , в качестве Middle frontend разработчика. Сейчас, мой фокус
              внимания сосредоточен на создании пользовательских интерфейсов для
              нескольких продуктов (
              <a href="https://fairmay.com/" target="_blank">
                Banking
              </a>
              ,{" "}
              <a href="https://zharpizza.ru/" target="_blank">
                FoodTech
              </a>
              )
            </p>
            <p>Вот несколько технологии, с которыми я недавно работал:</p>
            <ul className="about__list">
              {listItems.map((item, index) => (
                <li className="about__list-item" key={index}>
                  {item}
                </li>
              ))}
            </ul>
          </div>
          <div className="about__photo-part">
            <DotsAbout />
            <img
              src={avatarImage}
              className="about__avatar-image"
              alt="avatar"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default About;
