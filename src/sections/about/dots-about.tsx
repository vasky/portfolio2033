import React, { useEffect, useRef } from "react";
import { fabric } from "fabric";
import { Rect, StaticCanvas } from "fabric/fabric-impl";
import { clamp, lerp, lerpColor } from "../../utils/utils";

let _timer = 0;
const _timerStep = 0.7;
const _timerMaxValue = 1;
const _maxPointHeight = 20;
const _maxPointDistance = 300;

let canvasDots: StaticCanvas | undefined;
const circles: (Rect & {
  initTop?: number;
  initLeft?: number;
  get?: unknown;
})[] = [];

let dotsVisible = false;

export const DotsAbout = () => {
  const canvasEl = useRef<HTMLCanvasElement>(null);
  const bundCanvas = useRef<DOMRect | null>(null);

  const handleScroll = () => {
    if (canvasEl.current) {
      bundCanvas.current = canvasEl.current.getBoundingClientRect();
    }
  };

  const handleMouseMove = (e: MouseEvent) => {
    if (!bundCanvas.current && canvasEl.current) {
      bundCanvas.current = canvasEl.current.getBoundingClientRect();
    }

    const { clientX, clientY, offsetX, offsetY } = e;
    _timer += _timerStep;

    if (_timer > _timerMaxValue && bundCanvas.current && dotsVisible) {
      _timer = 0;

      for (let i = 0; i < circles.length; i++) {
        let nextHeight = 6;
        const currentHeight = circles[i].get("height") || 0;
        const left = circles[i].get("left") || 0;
        const top = circles[i].get("top") || 0;
        const initLeft = circles[i].initLeft || 0;
        const initTop = circles[i].initTop || 0;

        const nextTop = lerp(top, initTop, 0.05);
        const nextLeft = lerp(left, initLeft, 0.05);

        const deltaX = Math.abs(bundCanvas.current.x + left - clientX);
        const deltaY = Math.abs(bundCanvas.current.y + top - clientY);

        const deltaXColor = deltaX / 1000;
        const nextRotate =
          Math.atan2(
            clientY - (bundCanvas.current.y + initTop + currentHeight / 2),
            clientX - (bundCanvas.current.x + initLeft + 3)
          ) *
            (180 / Math.PI) +
          90;

        let color =
          deltaX > 900
            ? "#62FAD6"
            : lerpColor("#fad462", "#62FAD6", deltaXColor);

        if (deltaX < _maxPointDistance && deltaY < _maxPointDistance) {
          nextHeight = clamp(
            ((_maxPointDistance - deltaX) / _maxPointDistance) *
              _maxPointHeight,
            6,
            _maxPointHeight
          );
        }

        circles[i]
          .set("top", nextTop)
          .set("left", nextLeft)
          .set("fill", color)
          .set("height", nextHeight);

        circles[i].rotate(nextRotate);
      }
      canvasDots?.renderAll();
    }
  };

  useEffect(() => {
    const observer = new IntersectionObserver(
      (sections) => {
        sections.forEach((section) => {
          if (section.isIntersecting && !dotsVisible) {
            dotsVisible = true;
            createDots();
          }
        });
      },
      {
        threshold: 0.1,
      }
    );

    if (canvasEl.current) {
      observer.observe(canvasEl.current);
    }

    window.addEventListener("mousemove", handleMouseMove);
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("mousemove", handleMouseMove);
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const createDots = () => {
    canvasDots = new fabric.StaticCanvas(canvasEl.current);

    for (let i = 0; i < 12; i++) {
      for (let d = 0; d < 20; d++) {
        const circle: Rect & {
          initTop?: number;
          initLeft?: number;
        } = new fabric.Rect({
          width: 4,
          height: 4,
          top: i * 20,
          left: d * 20,
          fill: "#62FAD6",
          borderColor: "green",
          strokeWidth: 0,
          rx: 3,
          ry: 3,
          centeredRotation: true,
        });

        circle.initTop = i * 20;
        circle.initLeft = d * 20;

        setTimeout(() => {
          circles.push(circle);
          canvasDots?.add(circle);
        }, 5 * i * d);
      }
    }
  };

  return (
    <div className="about__dots">
      <canvas width="315" height="270" ref={canvasEl} />
    </div>
  );
};
