import React, { useEffect, useRef } from "react";
import Button from "../../components/button/button";
import "./first-block.scss";
import { animateScroll as scroll, Link } from "react-scroll";

const FirstBlock = () => {
  const firstBlockRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (firstBlockRef.current) {
      window.addEventListener("mousemove", handleMouseMove);
    }

    return () => window.removeEventListener("mousemove", handleMouseMove);
  }, []);

  const handleMouseMove = (e: MouseEvent) => {
    if (firstBlockRef.current) {
      const { clientX, clientY } = e;

      const backgroundX =
        clientX / 30 + firstBlockRef.current.clientWidth * -0.05;

      firstBlockRef.current.style.backgroundPosition = `${backgroundX}px ${
        clientY / 50
      }px`;
    }
  };

  return (
    <div className="first-block" ref={firstBlockRef}>
      <div className="container first-block__content">
        <span className="first-block__caption">Привет, меня зовут</span>
        <h1>Вадим Вдовиченко.</h1>
        <h1 className="h1--subrow first-block__subtitle">
          Frontend React + ts developer.
        </h1>

        <p className="first-block__description">
          Я frontend разработчик, с коммерческим опытом в 3.5 года. Нравится
          создавать что-то интересное и полезное, использовать новые технологии
          и принимать челленджи.
        </p>

        <Link
          to="about"
          smooth={true}
          offset={0}
          duration={1000}
          className="first-block__action-link"
        >
          <Button
            text="Ясно,  идем дальше!"
            className="first-block__action-btn"
          />
        </Link>
      </div>
    </div>
  );
};

export default FirstBlock;
