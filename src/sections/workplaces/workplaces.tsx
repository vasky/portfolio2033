import React from "react";
import "./workplaces.scss";
import SectionTitle from "../../components/section-title/section-title";
import InfoTabs from "../../components/info-tabs/info-tabs";
import { useInView } from "react-intersection-observer";
import classNames from "classnames";

const workplacesData = [
  {
    companyName: "HIPLABS",
    specialization: "Frontend разработчик",
    hashtag: "hiplabs",
    period: "Дек. 2021  -  по настоящее время",
    descriptionItems: [
      "Разработка новых проектов с нуля так и поддержка существующих.",
      "Настройка проектов: описание DockerFile, Webpack, Typescript config, Eslint, Prettier",
      "Проведение CodeReview",
      "Участие в обсуждение жизни проектов",
    ],
  },
  {
    companyName: "СКБ “Контур”",
    specialization: "Frontend разработчик",
    hashtag: "skbkontur",
    period: "Окт. 2021  -  Дек. 2021",
    descriptionItems: [
      "Разработка интфрейса, для внутренного сервиса компании, деплоя проектов для всей корпорации",
      "Разработка компонентов UI и исправление багов",
      "Большая команда, львиная доля бэкендеров.",
    ],
  },
  {
    companyName: "МА „МАЛИНА“",
    specialization: "Frontend разработчик",
    hashtag: "mediamalina",
    period: "Фев. 2021 - Окт. 2021",
    descriptionItems: [
      "Верстка и разработка frontend новых сайтов",
      "Настройка проектов: webpack, gulp",
      "Работа в команде из 5 разработчиков(Codereview, gitflow)",
      "Разработка UI для внутренного продукта компании",
    ],
  },
  {
    companyName: "НОУТ - 45",
    specialization: "Веб-разработчик",
    hashtag: "nout-45",
    period: "Янв. 2020  -  Фев. 2021",
    descriptionItems: [
      "Поддержка сайта компании, разработка функционала  страниц к промо акциям, создание и верстка новых страниц, создание лендингов под ключ.",
      "Работа с JS,  React, Git, SMC: Wordpress, Bitrix.",
      "Команда: 2 разработчика(fullstack + back),  дизайнер, продуктолог.",
    ],
  },
];

const Workplaces = () => {
  const { ref, entry } = useInView({
    threshold: 0.5,
    triggerOnce: true,
  });

  return (
    <div
      className={classNames("workplaces", {
        "fade-show": entry?.isIntersecting,
      })}
      id="workplaces"
      ref={ref}
    >
      <div className="container">
        <SectionTitle sectionText="Where I’ve Worked" sectionNumber="02." />
        <InfoTabs items={workplacesData} />
      </div>
    </div>
  );
};

export default Workplaces;
