import React, { useState } from "react";
import FirstBlock from "./sections/first-block/first-block";
import Layout from "./components/layout/layout";
import About from "./sections/about/about";
import Workplaces from "./sections/workplaces/workplaces";
import Projects from "./sections/projects/projects";
import Contacts from "./sections/contacts/contacts";

function App() {
  return (
    <Layout>
      <FirstBlock />
      <About />
      <Workplaces />
      <Projects />
      <Contacts />
    </Layout>
  );
}

export default App;
