import React, { useEffect, useState } from "react";
import "./menu.scss";
import { Link } from "react-scroll";
import classNames from "classnames";
import { CSSTransition } from "react-transition-group";

const menuItems = [
  {
    text: "Обо мне",
    to: "about",
  },
  {
    text: "Опыт",
    to: "workplaces",
  },
  {
    text: "Работа",
    to: "projects",
  },
  {
    text: "Контакты",
    to: "contacts",
  },
];

const Menu = () => {
  const [visibleMenu, setVisibleMenu] = useState(false);

  const onResize = () => {
    setVisibleMenu(window.innerWidth > 768);
  };

  useEffect(() => {
    onResize();
    window.addEventListener("resize", onResize);
    return () => window.removeEventListener("resize", onResize);
  }, []);

  return (
    <div className="menu-wrapper">
      <label htmlFor="open-menu-btn" className="open-menu-button">
        <input
          checked={visibleMenu}
          type="checkbox"
          id="open-menu-btn"
          onClick={() => setVisibleMenu((prevState) => !prevState)}
        />
        <span></span>
        <span></span>
        <span></span>
      </label>

      <CSSTransition
        in={visibleMenu}
        mountOnEnter
        unmountOnExit
        classNames={"menu-content-wrapper"}
        timeout={1000}
      >
        <div className="menu-content-wrapper">
          <a
            className="menu-wrapper__substrate"
            onClick={() => setVisibleMenu(false)}
          />
          <ul
            className={classNames("menu", {
              "menu--mobile-visible": visibleMenu,
            })}
          >
            {menuItems.map((item, index) => (
              <li className="menu__item" key={index}>
                <Link
                  to={item.to}
                  onClick={() => setVisibleMenu(false)}
                  smooth={true}
                  offset={0}
                  duration={1000}
                >
                  <span className="menu__item-index">
                    0{index + 1} <span className="menu__item-dot">.</span>
                  </span>
                  {item.text}
                </Link>
              </li>
            ))}
          </ul>
        </div>
      </CSSTransition>
    </div>
  );
};

export default Menu;
