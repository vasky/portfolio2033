import React from "react";
import "./open-project.scss";
import { ProjectCardItem } from "../project-card/project-card";
import { CSSTransition } from "react-transition-group";
import Button from "../button/button";
import CrossIcon from "./../../assets/images/cross-icon.svg";

type OpenProject = {
  visible: boolean;
  project?: ProjectCardItem;
  onClose: () => void;
};

const OpenProject: React.FC<OpenProject> = ({ visible, onClose, project }) => {
  return (
    <CSSTransition
      in={visible}
      mountOnEnter
      unmountOnExit
      classNames={"open-project-wrapper"}
      timeout={500}
    >
      <div className="open-project">
        <a className="open-project__substrate" onClick={onClose} />
        <aside className="open-project__aside">
          <div className="open-project__content">
            <div className="open-project__top open-project__group">
              <button onClick={onClose} className="open-project__cross-button">
                <img src={CrossIcon} alt="Close icon" />
              </button>
              <div className="open-project__company-name">{project?.name}</div>
              <div className="open-project__company-description">
                {project?.companyDescription}
              </div>
            </div>

            <div className="open-project__stack open-project__group">
              <div className="open-project__title">Стэк:</div>
              <ul className="open-project__stack-list">
                {project?.stack.map((stackItem, index) => (
                  <li className="open-project__stack-item" key={index}>
                    {stackItem}
                  </li>
                ))}
              </ul>
            </div>

            <div className="open-project__team open-project__group">
              <div className="open-project__title">Команда:</div>
              <p className="open-project__text">{project?.team}</p>
            </div>

            <div className="open-project__tasks open-project__group">
              <div className="open-project__title">Задачи:</div>
              <p className="open-project__text">{project?.tasks}</p>
            </div>

            <div className="open-project__links open-project__group">
              <div className="open-project__title">Ссылки:</div>
              <ul className="open-project__links-list">
                {project?.links.map((linkItem, index) => (
                  <a
                    key={index}
                    href={linkItem}
                    className="open-project__link-item"
                    target="_blank"
                  >
                    {linkItem}
                  </a>
                ))}
              </ul>
            </div>
          </div>

          <div className="open-project__controls">
            <Button text="Закрыть" onClick={onClose} />
          </div>
        </aside>
      </div>
    </CSSTransition>
  );
};

export default OpenProject;
