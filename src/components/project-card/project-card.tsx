import React from "react";
import "./project-card.scss";
import FolderIcon from "./../../assets/images/folder-icon.svg";
import LinkIcon from "./../../assets/images/link-icon.svg";

export type ProjectCardItem = {
  name: string;
  companyDescription: string;
  stack: string[];
  team: string;
  tasks: string;
  links: string[];
  cardLink: string;
  shortDescription: string;
  id: number;
};

const ProjectCard: React.FC<
  ProjectCardItem & { onClick: (project: ProjectCardItem) => void }
> = ({ onClick, ...project }) => {
  const { name, cardLink, shortDescription, stack, id } = project;

  return (
    <span className="project-card-wrapper">
      <button className="project-card" onClick={() => onClick(project)}>
        <div className="project-card__main-part">
          <div className="project-card__top">
            <div className="project-card__folder-icon">
              <img src={FolderIcon} alt="folder icon" />
            </div>
            <div className="project-card__card-link">
              <a
                href={cardLink}
                target="_blank"
                onClick={(e) => e.stopPropagation()}
              >
                <img src={LinkIcon} alt="link" />
              </a>
            </div>
          </div>
          <div className="project-card__company-name">{name}</div>
          <div className="project-card__description">{shortDescription}</div>
        </div>
        <div className="project-card__stack">
          <ul className="project-card__stack-list">
            {stack.map((item, index) => (
              <li
                className="project-card__stack-item"
                key={id + "name" + index}
              >
                {item}
              </li>
            ))}
          </ul>
        </div>
      </button>
    </span>
  );
};

export default ProjectCard;
