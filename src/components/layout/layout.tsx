import React from "react";
import "./layouts.scss";
import Header from "../header/header";

type LayoutProps = {
  children: React.ReactNode;
};

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <div className="layout">
      <main>
        <Header />
        {children}
      </main>
    </div>
  );
};

export default Layout;
