import React from "react";
import "./sections-title.scss";

type SectionTitleProps = {
  sectionNumber: string;
  sectionText: string;
  withoutLine?: boolean;
};

const SectionTitle: React.FC<SectionTitleProps> = ({
  sectionNumber,
  sectionText,
  withoutLine,
}) => {
  return (
    <div
      className={`section-title ${
        withoutLine ? "section-title--without-line" : ""
      }`}
    >
      <div className="section-title__text">
        <span className="section-title__number">{sectionNumber}</span>
        {sectionText}
      </div>
      <span className="section-title__line" />
    </div>
  );
};

export default SectionTitle;
