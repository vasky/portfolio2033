import React, { useEffect, useRef, useState } from "react";
import Menu from "../menu/menu";
import "./header.scss";

const Header = () => {
  const scrollPosition = useRef(0);
  const [hideHeader, setHideHeader] = useState(false);
  useEffect(() => {
    window.addEventListener("scroll", handlerScroll);

    return () => window.removeEventListener("scroll", handlerScroll);
  }, []);

  const handlerScroll = (e: Event) => {
    if (window.innerWidth > 768) {
      const scrollY = window.scrollY;

      setHideHeader(scrollY > scrollPosition.current);
      scrollPosition.current = window.scrollY;
    }
  };

  return (
    <div className={`header ${hideHeader ? "header--hide" : ""}`}>
      <div className={`container`}>
        <Menu />
      </div>
    </div>
  );
};

export default Header;
