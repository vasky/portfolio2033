import React, { MouseEventHandler, useRef, useState } from "react";
import "./info-tabs.scss";

type InfoTabsItem = {
  companyName: string;
  specialization: string;
  hashtag: string;
  period: string;
  descriptionItems: string[];
};

type InfoTabs = {
  items: InfoTabsItem[];
};

const InfoTabs: React.FC<InfoTabs> = ({ items }) => {
  const [topScroll, setTopScroll] = useState(0);
  const [activeTab, setActiveTab] = useState(0);

  const handleTabClick = (
    e: React.MouseEvent<HTMLButtonElement>,
    index: number
  ) => {
    const targetElement = e.target as HTMLElement;
    const targetParentElement = targetElement?.parentNode
      ?.parentNode as HTMLElement;
    if (targetElement) {
      const nextScroll =
        targetElement?.getBoundingClientRect().top -
        targetParentElement?.getBoundingClientRect().top;

      setTopScroll(nextScroll);
    }
    setActiveTab(index);
  };

  return (
    <div className="info-tabs">
      <div className="info-tabs__tabs">
        <ul className="info-tabs__tabs-list">
          <div className="info-tabs__scrollbar">
            <div
              className="info-tabs__scrollbar-thumb"
              style={{ top: topScroll, left: activeTab * 140 }}
            />
          </div>
          {items?.map((item, index) => (
            <li
              key={index}
              className={`info-tabs__item ${
                index === activeTab ? "info-tabs__item--active" : ""
              }`}
            >
              <button
                className="info-tabs__item-btn"
                onClick={(e) => handleTabClick(e, index)}
              >
                {item.companyName}
              </button>
            </li>
          ))}
        </ul>
      </div>
      <div className="info-tabs__content" key={activeTab}>
        <div className="info-tabs__content-top">
          <span className="info-tabs__company-name">
            {items[activeTab].specialization}
          </span>
          <span className="info-tabs__company-hashtag">
            @
            <a href="#" target="_blank">
              {items[activeTab].hashtag}
            </a>
          </span>
        </div>
        <div className="info-tabs__dates">{items[activeTab].period}</div>
        <div className="info-tabs__description">
          <ul className="info-tabs__description-list">
            {items[activeTab].descriptionItems.map((descriptionItem, index) => (
              <li className="info-tabs__description-item" key={index}>
                {descriptionItem}
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default InfoTabs;
