import React from "react";
import "./button.scss";

type ButtonProps = {
  onClick?: () => void;
  text: string;
  className?: string;
};

const Button: React.FC<ButtonProps> = ({ onClick, text, className }) => {
  return (
    <button
      className={`button ${className ? className : ""}`}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default Button;
